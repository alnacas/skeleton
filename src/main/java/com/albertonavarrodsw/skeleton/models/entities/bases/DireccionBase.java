package com.albertonavarrodsw.skeleton.models.entities.bases;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class DireccionBase {

    private String direccion;

    private String codigoPostal;

    private String poblacion;

    private String provincia;

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDireccionCompleta() {
        return String.format("%s, %s, %s (%s)", direccion, codigoPostal, poblacion, provincia);
    }

}
