package com.albertonavarrodsw.skeleton.models.entities.enums.converters;

import com.albertonavarrodsw.skeleton.models.entities.enums.TipoIvaBase;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class TipoIvaEnumConverter implements AttributeConverter<TipoIvaBase, Integer> {
    @Override
    public Integer convertToDatabaseColumn(TipoIvaBase tipoIva) {
        return tipoIva.getValor();
    }

    @Override
    public TipoIvaBase convertToEntityAttribute(Integer valor) {
        if (valor == null) {
            throw new IllegalArgumentException();
        }

        return Stream.of(TipoIvaBase.values())
                .filter(ti -> ti.getValor().equals(valor))
                .findFirst()
                .orElseThrow(IllegalAccessError::new);
    }
}
