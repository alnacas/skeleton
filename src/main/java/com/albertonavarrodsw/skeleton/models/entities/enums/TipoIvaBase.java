package com.albertonavarrodsw.skeleton.models.entities.enums;

public enum TipoIvaBase {
    NO(0, "NO"),
    SI(1, "SI"),
    RECARGO(2, "RECARGO");

    private final Integer valor;

    private final String nombre;

    TipoIvaBase(Integer valor, String nombre) {
        this.valor = valor;
        this.nombre = nombre;
    }

    public Integer getValor() {
        return valor;
    }

    public String getNombre() {
        return nombre;
    }

}
