package com.albertonavarrodsw.skeleton.models.entities;

import com.albertonavarrodsw.skeleton.models.entities.bases.DireccionBase;

import javax.persistence.*;

@Entity
@Table(name = "direcciones_fiscales")
public class DireccionFiscal extends DireccionBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
