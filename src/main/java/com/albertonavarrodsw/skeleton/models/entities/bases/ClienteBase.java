package com.albertonavarrodsw.skeleton.models.entities.bases;

import com.albertonavarrodsw.skeleton.models.entities.DireccionFiscal;
import com.albertonavarrodsw.skeleton.models.entities.enums.TipoIvaBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "clientes")
@Inheritance(strategy = InheritanceType.JOINED)
public class ClienteBase {

    @Id
    private Long id;

    @Column(name = "razon_social")
    private String razonSocial;

    private String nif;

    @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private DireccionFiscal direccionFiscal;

    private String alias;

    @Column(name = "fecha_alta")
    @Temporal(value = TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fechaAlta;

    @Column(name = "fecha_baja")
    @Temporal(value = TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fechaBaja;

    private TipoIvaBase iva;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    private List<ClienteContactoBase> contactos;

    @PrePersist
    public void prePersist() {
        fechaAlta = new Date();
    }

}
